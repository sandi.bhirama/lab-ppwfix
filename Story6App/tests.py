from django.test import TestCase, Client
from django.urls import resolve
from .views import addStatus
from .views import showProfile
from .views import deleteStatus
from .models import Status
from .forms import add_status

import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# from webdriver_manager.chrome import ChromeDriverManager


class Story6test(TestCase):
    def test_Story6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_Story6_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_Story6_using_addStatus_func(self):
        found = resolve('/')
        self.assertEqual(found.func,addStatus)

    def test_status_created(self):
        contoh_judul =  "Hello World"
        contoh_isi = "INI ISI"
        early_count = Status.objects.count()
        status_test = Status.objects.create(judul_Status=contoh_judul, isi_Status=contoh_isi)
        last_count = Status.objects.count()
        self.assertEqual(early_count + 1, last_count)  
        self.assertEqual(status_test.judul_Status, contoh_judul)
        self.assertEqual(status_test.isi_Status, contoh_isi)

    
    def test_model_can_create_new_status(self):
            new_activity = Status.objects.create(judul_Status='ini judul',isi_Status='Aku mau latihan ngoding deh')
            counting_all_available_activity = Status.objects.all().count()
            self.assertEqual(counting_all_available_activity,1)

    def test_can_save_a_POST_request(self):
        response = self.client.post('/', data={'judul_Status': 'ini judul', 'isi_Status' : 'aku mau latihan ngoding dehh'})
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('aku mau latihan ngoding dehh', html_response)

    def test_Story6_post_success_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/', {'judul_Status': test, 'isi_Status': test})
            self.assertEqual(response_post.status_code, 302)
    
            response= Client().get('/')
            html_response = response.content.decode('utf8')
            self.assertIn(test, html_response)

    def test_Story6_post_error_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/', {'judul_Status': '', 'isi_Status': ''})
            self.assertEqual(response_post.status_code, 302)
    
            response= Client().get('/')
            html_response = response.content.decode('utf8')
            self.assertNotIn(test, html_response)

    # def test_string_representation(self):
    #     string = Status(judul_Status="ngoding kuy")
    #     self.assertEqual(str(string), string.judul_Status)

    def test_create_anything_model(self, judul='ini judul', isi='ini isi dtatus'):
    		return Status.objects.create(judul_Status = judul, isi_Status=isi)


    # TEST UNTUK CHALLANGE

    def test_Story6_Challange_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_Story6_Challange_using_index_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'Profile.html')

    def test_Story6_Challange_using_addStatus_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func,showProfile)

# functional test untuk story 7


class test_new_visitor_story6(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(3) 
        super(test_new_visitor_story6,self).setUp()

    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()

    def test_write_form(self):
        self.browser.get('http://sandi-story7.herokuapp.com/')
        time.sleep(2)

        judulStatus_form = self.browser.find_element_by_id('id_judul_Status')
        isiStatus_form = self.browser.find_element_by_id('id_isi_Status')
        

        judulStatus_form.send_keys('Ini judul Status Brooww')
        isiStatus_form.send_keys('Ini isi dari status cuyyy')

        judulStatus_form.submit()

        self.assertIn('Ini judul Status Brooww', self.browser.page_source)
        self.assertIn('Ini isi dari status cuyyy', self.browser.page_source)

        time.sleep(2)
        self.browser.find_element_by_name('Delete_Button').click()
        time.sleep(2)
        self.browser.find_element_by_name('Profile_Button').click()

        time.sleep(2)

    # functional test untuk Challange Story 7
   
    def test_position_and_css_form(self):
        self.browser.get('http://sandi-story7.herokuapp.com')
        time.sleep(1)

        delete_status = self.browser.find_element_by_name('Delete_Button')
        profile_button = self.browser.find_element_by_name('Profile_Button')

        self.assertIn('Delete My Status', delete_status.text)
        self.assertIn('Profile', profile_button.text)
        # self.assertEqual('http://sandi-story7.herokuapp.com/profile', )

        delete_button_use_class = "btn-primary" in delete_status.get_attribute("class")
        profile_button_use_class = "btn-primary" in profile_button.get_attribute("class")

        self.assertTrue(delete_button_use_class)
        self.assertTrue(profile_button_use_class)


if __name__ == '__main__': 
    unittest.main(warnings='ignore') 






    

 


