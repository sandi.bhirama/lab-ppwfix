from django import forms
from .models import Status
from django.forms import ModelForm


class add_status(forms.ModelForm):
    class Meta:
        model = Status
        fields = ["judul_Status", "isi_Status"]
        widgets = {
            "judul_Status" : forms.TextInput(attrs={'class' : 'form-control'}),
            "isi_Status" : forms.TextInput(attrs={'class' : 'form-control'}),
        }