from django.db import models

class Status(models.Model):
	judul_Status = models.CharField(max_length=255)
	isi_Status = models.TextField(max_length=300)
	created_at = models.DateTimeField(auto_now_add=True)

	# def __str__(self):
	# 	return "{}".format(self.judul_Status)
