"""Lab4Ku URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.conf.urls import url
from . import views
from Lab4App1 import views as appViews

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',appViews.Home),
    url(r'^profile/$', appViews.Profile),
    url(r'^contact/$',  appViews.Contact),
    url(r'^home/$', appViews.Home),
    url(r'^form/$', appViews.Form),
    url(r'^result/$', appViews.Result),
    url(r'^add/$', appViews.Add),
    url(r'^result/delete$', appViews.Delete),
    url(r'^subscribe/$', appViews.Subscribe_Views),
    url(r'^subscribe/check_email/$', appViews.check_email),
    url(r'^get-subs-list/$', appViews.subs_list_json),
    url(r'^subscribe/delete/$', appViews.unsubscribe),
]
