from django.test import TestCase, Client
from django.urls import resolve
from .views import Home,Profile,Form,Contact,Add,Delete,Subscribe_Views,check_email

class Story10test(TestCase):
	def test_Story10_url_is_exist(self):
	    response = Client().get('/')
	    self.assertEqual(response.status_code,200)

	def test_1Story10_using_index_template(self):
	    response = Client().get('/')
	    self.assertTemplateUsed(response, 'Home.html')

	def test_2Story10_using_index_template(self):
	    response = Client().get('/profile/')
	    self.assertTemplateUsed(response, 'ProfileS.html')

	def test_3Story10_using_index_template(self):
	    response = Client().get('/contact/')
	    self.assertTemplateUsed(response, 'Contact.html')

	def test_4Story10_using_index_template(self):
	    response = Client().get('/home/')
	    self.assertTemplateUsed(response, 'Home.html')

	def test_5Story10_using_index_template(self):
	    response = Client().get('/form/')
	    self.assertTemplateUsed(response, 'Form.html')

	def test_6Story10_using_index_template(self):
	    response = Client().get('/result/')
	    self.assertTemplateUsed(response, 'Result.html')

	def test_7Story10_using_index_template(self):
	    response = Client().get('/add/')
	    self.assertTemplateUsed(response, 'Add.html')

	def test_8Story10_using_index_template(self):
	    response = Client().get('/subscribe/')
	    self.assertTemplateUsed(response, 'subs.html')

	def test_Story10_using_func(self):
	    found = resolve('/')
	    self.assertEqual(found.func,Home)

	# def test_story9_using_book_data_func(self):
	# 	found = resolve('/api/books')
	# 	self.assertEqual(found.func,book_data)