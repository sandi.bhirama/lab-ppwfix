from django.db import models
from django.utils import timezone
from datetime import datetime

class Jadwal(models.Model):
	judul = models.CharField(max_length=255)
	hariTanggal = models.DateField()
	jam = models.TimeField()
	kegiatan = models.CharField(max_length=255)
	kategori = models.CharField(max_length=255)
	tempat = models.CharField(max_length=255)

	def __str__(self):
		return "{}".format(self.judul)

class Subscribe(models.Model):
	email = models.EmailField()
	nama = models.CharField(max_length=150)
	password1 = models.CharField(max_length=100)

	def as_dict(self):
		return{
			"email" : self.email,
			"nama" : self.nama,
			"password1" : self.password1,
		}

	def __str__(self):
		return "{}".format(self.email)