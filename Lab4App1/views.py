from django.http import HttpResponse
from django.shortcuts import render
from .models import Jadwal
from .models import Subscribe
from .forms import add_form
from .forms import subs_form
from django.contrib import messages

from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core import serializers
from django.db import IntegrityError
from django.views.decorators.csrf import csrf_exempt


def Home(request):
	return render(request,'Home.html')

def Profile(request):
	return render(request,'ProfileS.html')

def Form(request):
	return render(request,'Form.html')

def Contact(request):
	return render(request, 'Contact.html')

def Result(request):
	jadwals = Jadwal.objects.all()
	context = {
		'Jadwals': jadwals,
	}
	return render(request,'Result.html',context)

def Add(request):
	if request.method == "POST":
		form = add_form(request.POST)
		if form.is_valid():
			form_item = form.save(commit=True)
			form_item.save()
		return HttpResponseRedirect('/')
	else:
		form = add_form()
	return render(request, 'Add.html', {'form' : form}) 

def Delete(request):
	Jadwal.objects.all().delete()
	return HttpResponseRedirect('/result')
@csrf_exempt
def Subscribe_Views(request):
	all_subs = Subscribe.objects.all()
	subs_list = Subscribe.objects.all()
	if request.method == "POST":
		form = subs_form(request.POST)
		if form.is_valid():
			form_item = form.save(commit=True)
			form_item.save()
			form = subs_form()
	else:
		form = subs_form()
		
	return render(request, 'subs.html', {'form' : form, 'subs_list' : subs_list})

	# form = subs_form(request.POST)
	# if request.method == 'POST' and form.is_valid():
	#     data = form.cleaned_data
	#     status_subscribe = True
	#     try:
	#         Subscribe.objects.create(**data)
	#     except:
	#         status_subscribe = False
	#     # return JsonResponse({'status_subscribe' : status_subscribe})
	# content = {'form': form}
	# return render(request, 'subs.html', content)
	


@csrf_exempt
def check_email(request):
	if request.method == 'POST':
		email = request.POST['email']
		nama = request.POST['nama']
		password1 = request.POST['password1']

		subs_filter = Subscribe.objects.filter(email = email)

		if len(subs_filter) > 0 :
			return JsonResponse({'message' : 'Email already used'})

		else:
			return JsonResponse({'message' : 'User Valid'})

	else:
		return JsonResponse({'message' : 'Something went wrong'})

def subs_list(request):
    subs_list = Subscribe.objects.all()
    response['subs_list'] = subs_list
    html = 'subs.html'
    return render(request, html, response)

def subs_list_json(request): # update
    subs = [obj.as_dict() for obj in Subscribe.objects.all()]
    return JsonResponse({"results": subs}, content_type='application/json')

@csrf_exempt
def unsubscribe(request):
	if request.method == "POST":
		email = request.POST['email']
		Subscribe.objects.filter(email =email).delete()
		return HttpResponseRedirect('/subscribe')
