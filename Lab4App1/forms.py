from django import forms
from .models import Jadwal
from .models import Subscribe
from django.forms import ModelForm


class add_form(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = ["judul", "hariTanggal", "jam", "kegiatan", "kategori", "tempat"]
        widgets = {
            "judul" : forms.TextInput(attrs={'class' : 'form-control'}),
            "hariTanggal" : forms.TextInput(attrs={'type':'date' ,'class' : 'form-control'}),
            "jam" : forms.TextInput(attrs={'type' : 'time' ,'class' : 'form-control'}),
            "kegiatan" : forms.TextInput(attrs={'class' : 'form-control'}),
            "kategori" : forms.TextInput(attrs={'class' : 'form-control'}),
            "tempat" : forms.TextInput(attrs={'class' : 'form-control'}),
        }

class subs_form(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = Subscribe
        fields = ["email","nama","password1"]
    # error_messages = {
    #     'required': 'Tolong isi input ini',
    #     'invalid': 'Isi input dengan valid',
    # }
    # attrs = {
    #     'class': 'form-control'
    # }
    # date_attrs = {
    #     'type' : 'date',
    #     'class' : 'todo-form-input',
    # }
    # tempat_attrs ={
    #     'type' : 'text',
    #     'class' : 'todo-form-input',
    #     'placeholder' : 'Tempat Kegiatan .. ',

    # }
    # kegiatan_attrs ={
    #     'type' : 'text',
    #     'class' : 'todo-form-input',
    #     'placeholder' : 'Nama Kegiatan .. '
    # }
    
    # judul = forms.CharField(label='Judul', required=True, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    # hariTanggal = forms.DateField(required=True, widget=forms.SelectDateWidget(attrs=attrs))
    # jam = forms.TimeField(required=True, widget=forms.TimeInput(attrs=attrs))
    # kegiatan = forms.CharField(label='Nama Kegiatan', widget=forms.Textarea(attrs=attrs), required=True)
    # kategori = forms.CharField(label='Kategori', widget=forms.Textarea(attrs=attrs), required=True)
    # tempat = forms.CharField(label='Tempat', widget=forms.Textarea(attrs=attrs), required=True)
