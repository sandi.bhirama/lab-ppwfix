from django.shortcuts import render
from django.http import JsonResponse
import urllib.request, json
import requests
# Create your views here.

def book_data(request,search):
    # try:
    # 	searching = request.GET["search"]
    # except:
    # 	search = "quilting"

    raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q='+search).json()
    # itemss =[]
    # for info in raw_data['items']:
    #     data = {}
    #     data['title'] = info['volumeInfo']['title']
    #     data['publisher'] = info['volumeInfo']['publisher']
    #     data['publishedDate'] = info['volumeInfo']['publishedDate']
    #     data['pageCount'] = info['volumeInfo']['pageCount']
    #     data['language'] = info['volumeInfo']['language']
    #     data['categories'] = info['volumeInfo']['categories']
    #     data['author'] = ", ".join(info['volumeInfo']['authors'])
        
    #     itemss.append(data)
    return JsonResponse(raw_data)


def Book(request):
	return render(request, 'ajaxBook.html')

