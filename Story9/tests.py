from django.test import TestCase, Client
from django.urls import resolve
from .views import Book, book_data

class Story8test(TestCase):
	def test_Story9_url_is_exist(self):
	    response = Client().get('/')
	    self.assertEqual(response.status_code,200)

	def test_Story9_using_index_template(self):
	    response = Client().get('/')
	    self.assertTemplateUsed(response, 'ajaxBook.html')

	def test_Story9_using_Book_func(self):
	    found = resolve('/')
	    self.assertEqual(found.func,Book)

	# def test_story9_using_book_data_func(self):
	# 	found = resolve('/api/books')
	# 	self.assertEqual(found.func,book_data)