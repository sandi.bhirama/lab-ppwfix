from django.conf.urls import url, include
from django.conf import settings
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.http import HttpResponse
from Story9 import views as story9views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',story9views.Book, name='book'),
    url(r'^api/books/(?P<search>\w+)/$', story9views.book_data),
    url(r'^login/$', auth_views.LoginView.as_view(), name='login'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    url(r'^logout/$', auth_views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),

]
