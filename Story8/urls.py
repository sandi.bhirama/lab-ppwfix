from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.conf.urls import url
from Story8 import views as story8views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',story8views.Index),

]