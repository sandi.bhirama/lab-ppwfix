from django.test import TestCase, Client
from django.urls import resolve
from .views import Index

import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Story8test(TestCase):
	def test_Story8_url_is_exist(self):
	    response = Client().get('/')
	    self.assertEqual(response.status_code,200)

	def test_Story8_using_index_template(self):
	    response = Client().get('/')
	    self.assertTemplateUsed(response, 'profileJS.html')

	def test_Story8_using_addStatus_func(self):
	    found = resolve('/')
	    self.assertEqual(found.func,Index)

class test_NewVisitor_Story8(unittest.TestCase):
	def setUp(self):
	    chrome_options = Options()
	    chrome_options.add_argument('--dns-prefetch-disable')
	    chrome_options.add_argument('--no-sandbox')
	    chrome_options.add_argument('--headless')
	    chrome_options.add_argument('disable-gpu')
	    service_log_path = "./chromedriver.log"
	    service_args = ['--verbose']
	    self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
	    self.browser.implicitly_wait(3) 
	    super(test_NewVisitor_Story8,self).setUp()

	def tearDown(self):
	    self.browser.implicitly_wait(3)
	    self.browser.quit()

	def test_day_and_night_click(self):
	    self.browser.get('http://sandi-story8.herokuapp.com/')
	    time.sleep(2)

	    self.browser.find_element_by_id('night').click()
	    self.browser.find_element_by_id('Day').click()
	   
	    time.sleep(2)

	def test_css(self):
		self.browser.get('http://sandi-story8.herokuapp.com')
		time.sleep(1)

		day_button = self.browser.find_element_by_id('Day')
		night_button = self.browser.find_element_by_id('night')

		self.assertIn('Day Mode', day_button.text)
		self.assertIn('Night Mode', night_button.text)

		activity = self.browser.find_element_by_name('activity')
		organization = self.browser.find_element_by_name('organiztion')
		achievement = self.browser.find_element_by_name('achievement')

		activity_use_class = "accordion" in activity.get_attribute("class")
		organization_use_class = "accordion" in organization.get_attribute("class")
		achievement_use_class = "accordion" in achievement.get_attribute("class")

		self.assertTrue(activity_use_class)
		self.assertTrue(organization_use_class)
		self.assertTrue(achievement_use_class)

if __name__ == '__main__': 
	unittest.main(warnings='ignore') 
